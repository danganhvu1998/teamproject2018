from django.shortcuts import render, redirect
from django.http import Http404
from django.utils import timezone
from django.http import JsonResponse as jsonResponse

from django.contrib.auth.models import User
from .models import Book, Comment

def index(request):
    books = Book.objects.order_by('posted_at')[:3]
    context ={
        'books':books
    }
    return render(request, 'iniadRead/index.html',context)

def about(request):
    return render(request, 'iniadRead/about.html')

#Books Controller
def addBook(request):
    if request.method == 'POST':
        book = Book(
            title=request.POST['title'], 
            intro=request.POST['intro'], 
            image_url=request.POST['image_url'], 
            posted_at=timezone.now(),
            status=1,
            user_id=request.user
        )
        book.save()
        return redirect("allBooks", 1)
    else:
        return render(request, 'booksCtrl/add.html')

def showAllBooks(request, page):
    books = Book.objects.order_by('posted_at')
    context = {
        'books': books
    }
    return render(request, 'booksCtrl/showAll.html', context)



def detailBook(request, book_id):
    if request.method == 'POST':
        comment = Comment(
            text=request.POST["comment"],
            book_id_id=book_id,
            user_id=request.user,
            posted_at = timezone.now()
        )
        comment.save()
        return redirect("detailBook", book_id)
    try:
        book = Book.objects.get(pk = book_id)
        comments = Comment.objects.order_by('-posted_at').filter(book_id = book).select_related()
    except Book.DoesNotExist:
        raise Http404('Book does not exist')
    context = {
        'book': book,
        'comments': comments,
    }
    #print(comments[0].user_id.first_name)
    return render (request, 'booksCtrl/detail.html', context)

def edit(request, book_id):
    try:
        book = Book.objects.get(pk = book_id)
    except:
        raise Http404('Book does not exist')
    
    if request.method == 'POST':
        book.title = request.POST['title']
        book.intro = request.POST['intro']
        book.image_url = request.POST['image_url']
        book.save()
    
    context = {
        'book' : book
    }
    return render (request,'booksCtrl/edit.html', context)
    
def searchBook(request):
    books = Book.objects.filter(title__contains=request.POST['title']).order_by('posted_at')
    context = {
        'books': books
    }
    return render(request, 'booksCtrl/showAll.html', context)